package com.etnetera.hr;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.time.Month;


/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptFrameworkTests
{

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule())
                                                    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @Autowired
    private JavaScriptFrameworkRepository repository;

    private void prepareData ()
    {

        JavaScriptFramework react =
                new JavaScriptFramework("ReactJS")
                        .setVersion(1L)
                        .setHypeLevel(10L)
                        .setDeprecationDate(LocalDate.of(2018, Month.JUNE, 27));

        JavaScriptFramework vue =
                new JavaScriptFramework("Vue.js")
                        .setVersion(2L)
                        .setHypeLevel(0L)
                        .setDeprecationDate(LocalDate.of(2016, Month.FEBRUARY, 1));

        repository.save(react);
        repository.save(vue);
    }

    private void prepareData2 ()
    {

        for(long i = 0; i < 10; i++)
        {
            JavaScriptFramework react =
                new JavaScriptFramework("ReactJS")
                        .setHypeLevel(10L)
                        .setDeprecationDate(LocalDate.of(2018, Month.JUNE, 27))
                        .setVersion(100+i);
            repository.save(react);
        }
    }


    @Test
    public void frameworksTest () throws Exception
    {
        prepareData();

        mockMvc.perform(get("/frameworks"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(2)))
               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].name", is("ReactJS")))
               .andExpect(jsonPath("$[1].id", is(2)))
               .andExpect(jsonPath("$[1].name", is("Vue.js")));
    }

    @Test
    public void frameworksTestAllAttributes () throws Exception
    {
        prepareData();
        mockMvc.perform(get("/frameworks"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(2)))
               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].name", is("ReactJS")))
               .andExpect(jsonPath("$[0].version", is(1)))
               .andExpect(jsonPath("$[0].hypeLevel", is(10)))
               .andExpect(jsonPath("$[0].deprecationDate", is("2018-06-27")))

               .andExpect(jsonPath("$[1].id", is(2)))
               .andExpect(jsonPath("$[1].name", is("Vue.js")))
               .andExpect(jsonPath("$[1].version", is(2)))
               .andExpect(jsonPath("$[1].hypeLevel", is(0)))
               .andExpect(jsonPath("$[1].deprecationDate", is("2016-02-01")));
    }

    @Test
    public void singleFrameworkValid () throws Exception
    {
        prepareData();

        mockMvc.perform(get("/frameworks/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.name", is("ReactJS")))
               .andExpect(jsonPath("$.version", is(1)))
               .andExpect(jsonPath("$.hypeLevel", is(10)))
               .andExpect(jsonPath("$.deprecationDate", is("2018-06-27")));
    }

    @Test
    public void singleFrameworkInvalid () throws Exception
    {
        prepareData();
        
        mockMvc.perform(get("/frameworks/3"))
               .andExpect(status().isNotFound());
    }


    @Test
    public void addFrameworkInvalid () throws Exception
    {
        JavaScriptFramework framework = new JavaScriptFramework();

        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$.errors", hasSize(1)))
               .andExpect(jsonPath("$.errors[0].field", is("name")))
               .andExpect(jsonPath("$.errors[0].message", is("NotEmpty")));


        framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$.errors", hasSize(1)))
               .andExpect(jsonPath("$.errors[0].field", is("name")))
               .andExpect(jsonPath("$.errors[0].message", is("Size")));

    }

    @Test
    public void addFrameworkValid () throws Exception
    {
        JavaScriptFramework framework = new JavaScriptFramework("Vue.js");

        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isCreated())
               .andExpect(redirectedUrlPattern("**/frameworks/1"));

        mockMvc.perform(get("/frameworks/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.name", is("Vue.js")));
    }

    @Test
    public void addFrameworkAlreadyExists () throws Exception
    {
        prepareData(); //Vue.js in version 2

        JavaScriptFramework framework = new JavaScriptFramework("Vue.js");

        Long id = repository.count() + 1;

        //Vue.js without version should be ok
        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isCreated())
               .andExpect(redirectedUrlPattern("**/frameworks/" + id.toString()));

        mockMvc.perform(get("/frameworks/" + id.toString()))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id", is(id.intValue())))
               .andExpect(jsonPath("$.name", is("Vue.js")));

        //Adding Vue.js again without version should cause an error
        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isConflict());


        //Adding Vue.js in version 2 should also cause an error
        framework.setVersion(2L);
        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isConflict());

        //And version 3 should be ok
        framework.setVersion(3L);
        id++;
        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isCreated())
               .andExpect(redirectedUrlPattern("**/frameworks/" + id.toString()));

        mockMvc.perform(get("/frameworks/" + id.toString()))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id", is(id.intValue())))
               .andExpect(jsonPath("$.name", is("Vue.js")));
    }
    @Test
    public void addFrameworkValidAllProperties () throws Exception
    {
        JavaScriptFramework framework =
                new JavaScriptFramework("Vue.js")
                        .setVersion(1L)
                        .setHypeLevel(42L)
                        .setDeprecationDate(LocalDate.of(2018,Month.JUNE, 28));


        mockMvc.perform(post("/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(framework)))
               .andExpect(status().isCreated())
               .andExpect(redirectedUrlPattern("**/frameworks/1"));

        mockMvc.perform(get("/frameworks/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.name", is("Vue.js")));
    }

    @Test
    public void deleteFramework () throws Exception
    {
        prepareData();

        mockMvc.perform(delete("/frameworks/1"))
               .andExpect(status().isNoContent());

        mockMvc.perform(delete("/frameworks/1"))
               .andExpect(status().isNotFound());

        mockMvc.perform(get("/frameworks/1"))
               .andExpect(status().isNotFound());

        mockMvc.perform(get("/frameworks/2"))
               .andExpect(status().isOk());
    }

    @Test
    public void updateFramework () throws Exception
    {
        prepareData();

        JavaScriptFramework vue = repository.findById(2L).get();
        vue.setHypeLevel(42L)
           .setVersion(420L);

        mockMvc.perform(put("/frameworks/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(vue)))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id", is(2)))
               .andExpect(jsonPath("$.name", is("Vue.js")))
               .andExpect(jsonPath("$.version", is(420)))
               .andExpect(jsonPath("$.hypeLevel", is(42)))
               .andExpect(jsonPath("$.deprecationDate", is("2016-02-01")));

    }


    @Test
    public void searchFramework () throws Exception
    {
        prepareData();

        mockMvc.perform(get("/frameworks?name=ReactJS"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(1)))
               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].name", is("ReactJS")))
               .andExpect(jsonPath("$[0].version", is(1)))
               .andExpect(jsonPath("$[0].hypeLevel", is(10)))
               .andExpect(jsonPath("$[0].deprecationDate", is("2018-06-27")));

        repository.deleteAll();
        prepareData2();

        mockMvc.perform(get("/frameworks?name=ReactJS"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(10)));
    }

    @Test
    public void testAll () throws Exception
    {
        prepareData();   //Create (Vue.js, v2), (ReactJS, v1)
        prepareData2();  //Create (ReactJS, v100) ... (ReactJS, v109)

        mockMvc.perform(get("/frameworks")) //There is 12 frameworks
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", hasSize(12)));

        mockMvc.perform(get("/frameworks/5")) //The 5th framework is (ReactJS, v5)
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id", is(5)))
               .andExpect(jsonPath("$.name", is("ReactJS")))
               .andExpect(jsonPath("$.version", is(102)));

        mockMvc.perform(delete("/frameworks/5")) //Delete (ReactJS, v5)
               .andExpect(status().isNoContent());

        mockMvc.perform(get("/frameworks/5")) //Check if deleted
               .andExpect(status().isNotFound());

        mockMvc.perform(put("/frameworks/5")//Update should not work
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(new JavaScriptFramework("Vue.js"))))
               .andExpect(status().isNotFound());

        mockMvc.perform(post("/add") //Create (Vue.js, null)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(new JavaScriptFramework("Vue.js"))))
               .andExpect(status().isCreated());

        mockMvc.perform(post("/add") //Create (Vue.js, null) again = conflict
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(new JavaScriptFramework("Vue.js"))))
               .andExpect(status().isConflict());

        mockMvc.perform(put("/frameworks/4") //Update (ReactJS, v101) -> (Vue.js, null) = conflict
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(new JavaScriptFramework("Vue.js"))))
               .andExpect(status().isConflict());

        mockMvc.perform(delete("/frameworks/4")) //Delete (ReactJS, v101)
                                .andExpect(status().isNoContent());

        mockMvc.perform(put("/frameworks/4") // ID 4 not found
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(new JavaScriptFramework("Vue.js"))))
               .andExpect(status().isNotFound());

        mockMvc.perform(get("/frameworks?name=ReactJS")) //there are 9 versions of ReactJS
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(9)));

        mockMvc.perform(get("/frameworks?name=ReactJS&version=103")) //and just one ReactJS in version 103
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(1)));

        mockMvc.perform(put("/frameworks/3") //Update (ReactJS, v100) -> (ReactJS, v2)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsBytes(
                                        new JavaScriptFramework("ReactJS").setVersion(2L))))
               .andExpect(status().isOk());
        mockMvc.perform(get("/frameworks?version=2")) // expect ReactJS and Vue.js v2
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(2)))
               .andExpect(jsonPath("$[0].name", is("Vue.js")))
               .andExpect(jsonPath("$[0].version", is(2)))
               .andExpect(jsonPath("$[1].name", is("ReactJS")))
               .andExpect(jsonPath("$[1].version", is(2)));
    }


}
