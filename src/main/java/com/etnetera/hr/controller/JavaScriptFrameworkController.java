package com.etnetera.hr.controller;

import com.etnetera.hr.rest.Errors;
import com.etnetera.hr.rest.ValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;


/**
 * Simple REST controller for accessing application logic.
 *
 * <table>
 * <tr>
 * <td>REST endpoint</td> <td>HTTP Method</td> <td>Description</td>
 * </tr>
 *
 * <tr>
 * <td>/frameworks</td> <td>GET</td> <td>Return a collection of frameworks. Request parameters name and
 * version can be used for searching. </td>
 * </tr>
 *
 * <tr>
 * <td>/frameworks/{id}</td> <td>GET</td> <td>Return framework detail for given framework {id}</td>
 * </tr>
 *
 * <tr>
 * <td>/add</td> <td>POST</td> <td>Add a new framework</td>
 * </tr>
 *
 * <tr>
 * <td>/frameworks/{id}</td> <td>DELETE</td> <td>Delete the framework with {id}</td>
 * </tr>
 *
 * <tr>
 * <td>/frameworks/{id}</td> <td>PUT</td> <td>Update the framework with {id}</td>
 * </tr>
 *
 * </table>
 *
 * @author Etnetera
 */
@RestController
public class JavaScriptFrameworkController extends EtnRestController
{

    private final JavaScriptFrameworkRepository repository;

    @Autowired
    public JavaScriptFrameworkController (JavaScriptFrameworkRepository _Repository)
    { repository = _Repository; }

    /**
     * Retrieve saved frameworks. By default returns all stored frameworks. To narrow down the selection, request
     * parameters name or version can be specified. In this case the result contains just frameworks that match given
     * criteria.
     *
     * @param _Name    Name of the framework.
     * @param _Version Version of the framework.
     * @return An iterable collection with frameworks.
     */
    @GetMapping(value = "/frameworks")
    public Iterable<JavaScriptFramework> frameworks
    (@RequestParam(value = "name", required = false) Optional<String> _Name,
     @RequestParam(value = "version", required = false) Optional<Long> _Version
    )
    {
        List<JavaScriptFramework> frameworks = new ArrayList<>(repository.findAll());
        _Name.ifPresent(_name -> frameworks.retainAll(repository.findAllByName(_name)));
        _Version.ifPresent(_version -> frameworks.retainAll(repository.findAllByVersion(_version)));
        frameworks.sort(Comparator.comparing(JavaScriptFramework::getId));
        return frameworks;
    }


    /**
     * Retrieve a single framework with specified ID.
     *
     * @param _Id Framework ID.
     * @return Response not found if there is no framework with the given ID. Otherwise response ok.
     */
    @GetMapping("/frameworks/{id}")
    public ResponseEntity<?> framework (@PathVariable("id") Long _Id)
    {
        Optional<JavaScriptFramework> framework = repository.findById(_Id);
        if(!framework.isPresent())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(createNotFoundError(_Id));

        return ResponseEntity.status(HttpStatus.OK).body(framework.get());
    }

    /**
     * Adds a new framework. Framework's name should be not null and at most 30 characters long.
     *
     * @param _Framework Framework to be added.
     * @return On success response created. On failure the response is either bad request (name is null or
     * longer than 30 characters) or conflict (the framework with given name and version already exists).
     */
    @PostMapping("/add")
    public ResponseEntity<?> addFramework (@RequestBody JavaScriptFramework _Framework, UriComponentsBuilder uriBuilder)
    {
        Errors errors = checkValidity(_Framework);
        if(errors.hasErrors())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);

        errors = checkConflict(_Framework);
        if(errors.hasErrors())
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errors);

        return ResponseEntity.status(HttpStatus.CREATED)
                             .location(uriBuilder.path("/frameworks/{id}")
                                                 .buildAndExpand(repository.save(_Framework).getId())
                                                 .toUri()).build();
    }

    /**
     * Deletes a framework from repository.
     *
     * @param _Id Specifies the framework to be deleted.
     * @return Response no content on success. If the framework with _Id does not exist, returns response bad request.
     */
    @DeleteMapping("/frameworks/{id}")
    public ResponseEntity<?> deleteFramework (@PathVariable("id") Long _Id)
    {
        if(!repository.findById(_Id).isPresent())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(createNotFoundError(_Id));

        repository.deleteById(_Id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * Updates a framework. All properties of the framework can be updated except its id.
     *
     * @param _Id        Specifies the framework to be updated.
     * @param _Framework Contains new properties of the updated framework
     * @return Response ok if the framework with _Id is present in the repository and new properties do not violate
     * basic rules of framework (see addFramework method). If the framework with _Id does not exist, response
     * not found is returned.
     */
    @PutMapping("/frameworks/{id}")
    public ResponseEntity<?> updateFramework (@PathVariable("id") Long _Id, @RequestBody JavaScriptFramework _Framework)
    {
        if(!repository.findById(_Id).isPresent())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(createNotFoundError(_Id));

        Errors errors = checkValidity(_Framework);
        if(errors.hasErrors())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);

        errors = checkConflict(_Framework);
        if(errors.hasErrors())
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errors);

        return ResponseEntity.status(HttpStatus.OK).body(repository.save(_Framework.setId(_Id)));
    }

    //Check framework validity - name is not null and shorter than 30 chars.
    private Errors checkValidity (JavaScriptFramework _Framework)
    {
        Errors errors = new Errors();

        if(_Framework.getName() == null)
            errors.add(new ValidationError("name", "NotEmpty"));

        else if(_Framework.getName().length() > 30)
            errors.add(new ValidationError("name", "Size"));

        return errors;
    }

    //Check if the framework with _Framework.getName() and _Framework.getVersion() exists or not.
    private Errors checkConflict (JavaScriptFramework _Framework)
    {
        Errors errors = new Errors();
        if(repository.findAllByName(_Framework.getName()).contains(_Framework))
        {
            errors.add(new ValidationError("name", _Framework.getName() + " already exists"));
            if(_Framework.getVersion() != null)
                errors.add(new ValidationError("version", _Framework.getVersion() + "already exists"));
        }
        return errors;
    }

    private Errors createNotFoundError (Long _Id)
    {
        return new Errors().add(new ValidationError("id", _Id.toString()));
    }

}
