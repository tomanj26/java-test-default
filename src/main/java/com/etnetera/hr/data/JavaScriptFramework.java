package com.etnetera.hr.data;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * Properties are: ID, name, version, deprecation date and hype level.
 *
 * @author Etnetera
 */
@Entity
public class JavaScriptFramework
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 30)
    private String name;

    private Long hypeLevel;

    private LocalDate deprecationDate;

    private Long version;


    public JavaScriptFramework ()
    { }

    public JavaScriptFramework (String _Name)
    { setName(_Name); }

    public Long getId ()
    { return id; }

    public JavaScriptFramework setId (Long _Id)
    {
        this.id = _Id;
        return this;
    }

    public String getName ()
    { return name; }

    public JavaScriptFramework setName (String _Name)
    {
        this.name = _Name;
        return this;
    }

    public Long getHypeLevel ()
    { return hypeLevel; }

    public JavaScriptFramework setHypeLevel (final Long _HypeLevel)
    {
        hypeLevel = _HypeLevel;
        return this;
    }

    public LocalDate getDeprecationDate ()
    { return deprecationDate; }

    public JavaScriptFramework setDeprecationDate (final LocalDate _DeprecationDate)
    {
        deprecationDate = _DeprecationDate;
        return this;
    }

    public Long getVersion ()
    { return version; }

    public JavaScriptFramework setVersion (final Long _Version)
    {
        version = _Version;
        return this;
    }

    @Override
    public String toString ()
    { return "JavaScriptFramework " +
             "[id="    + getId() +
             ", name=" + getName() +
             ", version =" + getVersion() +
             ", hypeLevel =" + getHypeLevel() +
             ", deprecationDate =" + getDeprecationDate() +
             "]"; }

    @Override
    public boolean equals (final Object _Object)
    {
        if(this == _Object)
            return true;

        if(_Object == null || getClass() != _Object.getClass())
            return false;

        final JavaScriptFramework framework = (JavaScriptFramework) _Object;
        return Objects.equals(getName(), framework.getName()) &&
               Objects.equals(getVersion(), framework.getVersion());
    }

}
