package com.etnetera.hr.repository;

import org.springframework.data.repository.CrudRepository;

import com.etnetera.hr.data.JavaScriptFramework;

import java.util.List;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
public interface JavaScriptFrameworkRepository extends CrudRepository<JavaScriptFramework, Long>
{
    List<JavaScriptFramework> findAll ();

    List<JavaScriptFramework> findAllByName (String _Name);

    List<JavaScriptFramework> findAllByVersion (Long _Version);
}
