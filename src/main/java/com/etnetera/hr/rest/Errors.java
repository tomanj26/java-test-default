package com.etnetera.hr.rest;

import java.util.ArrayList;
import java.util.List;

/**
 * Envelope for the validation errors. Represents JSON response.
 *
 * @author Etnetera
 */
public class Errors
{

    private List<ValidationError> errors;

    public Errors ()
    { errors = new ArrayList<>(); }

    public List<ValidationError> getErrors ()
    { return errors; }

    public void setErrors (List<ValidationError> errors)
    { this.errors = errors; }

    public Errors add (ValidationError _Error)
    {
        if(errors == null)
            errors = new ArrayList<>();
        errors.add(_Error);
        return this;
    }

    public boolean hasErrors ()
    { return !errors.isEmpty(); }

}
